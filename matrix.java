import java.util.*;
public class matrix {
	static Scanner scan=new Scanner(System.in);
	static int selec;
	static void MainMenu(){
		while(true){
			int choose;
			System.out.println("Matriks");
			System.out.println("***********************************");			
			System.out.println("1. Penjumlahan");
			System.out.println("2. Pengurangan");
			System.out.println("3. Perkalian");
			System.out.println("4. Keluar");
			System.out.println("***********************************");	
			System.out.print("Masukkan Pilihan : ");
			do{
				try{
					choose=scan.nextInt();
					if(choose!= 1 && choose!= 2 && choose!= 3  && choose!= 4 ){
						System.out.println("Masukkan pilihan yang tersedia");
						continue;
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan pilihan yang tersedia");
					scan.nextLine();
					continue;
				}
			}while(true);
			if(choose==4){
				System.exit(0);
			}
			switch(choose){
				case 1:
					addition();
					break;
				case 2:
					reduction();
					break;
				case 3:
					multiplication();
					break;	
			}	
		}
	}
	
	static void addition(){
		int row,column;
		do{
			System.out.println("Penjumlahan");
			System.out.println("***********************************");
			do{	
				try{				
					System.out.print("Masukkan jumlah baris matriks : ");
					row=scan.nextInt();
						if (row <1){
							throw new InputMismatchException();
						}
					System.out.print("Masukkan jumlah kolom matriks : ");
					column=scan.nextInt();
						if (column <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan angka lebih besar dari 0");
					scan.nextLine();
					continue;
				}
			}while(true);			
			double[][]matriks1= new double[row][column];
			double[][]matriks2= new double[row][column];
			double[][]hasil=new double[row][column];
			System.out.println("Matriks A");
			do{
				try{
					for(int indeksrow=0; indeksrow<row;indeksrow++){
						for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
							System.out.print("Masukkan nilai baris "+(indeksrow+1)+" dan Kolom "+(indekscolumn+1)+" : ");
							matriks1[indeksrow][indekscolumn]=scan.nextDouble();
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					scan.nextLine();
					continue;
				}	
			}while(true);
			System.out.println();
			for(int indeksrow=0; indeksrow<row;indeksrow++){
				for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
					System.out.printf("%.2f  ",matriks1[indeksrow][indekscolumn]);
				}System.out.println();
			}
			System.out.println();
			System.out.println("Matriks B");
			do{
				try{
					for(int indeksrow=0; indeksrow<row;indeksrow++){
						for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
							System.out.println("Masukkan nilai baris"+(indeksrow+1)+" dan Kolom "+(indekscolumn+1));
							System.out.print(">> ");
							matriks2[indeksrow][indekscolumn]=scan.nextDouble();
							hasil[indeksrow][indekscolumn]=matriks1[indeksrow][indekscolumn] + matriks2[indeksrow][indekscolumn];
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					scan.nextLine();
					continue;
				}	
			}while(true);
			
			System.out.println();
			for(int indeksrow=0; indeksrow<row;indeksrow++){
				for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
					
					System.out.printf("%.2f  ",matriks2[indeksrow][indekscolumn]);
				}System.out.println();
			}			
			System.out.println("\n");
			System.out.println("Hasil dari penjumlahan kedua matriks adalah : ");
			System.out.println();
			for(int indeksrow=0; indeksrow<row;indeksrow++){
				for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
					
					System.out.printf("%.2f  ",hasil[indeksrow][indekscolumn]);
				}System.out.println();
			}
			if(row != column){
				System.out.println("Matriks tersebut tidak memiliki determinan");
			}	
			else{
				
				if(row==2 && column==2){
					if(((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0]))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0])));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
				else if(row==3 && column==3){
					if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
			}	
			System.out.println();
			scan.nextInt();
			do{
				System.out.println("Tekan 1 Untuk ulangi dan 0 untuk keluar");
				selec=scan.nextInt();
				if (selec==0){
					 break;
				 }
				 else  if (selec==1){
					 break;
				 }
				 else{
					 System.out.println("Masukkan angka 1 atau 0");
					 continue;
				 }
			}while(true);
			 if (selec==0){
				 break;
			 }
			 else  if (selec==1){
				 continue;
			 }
		}while(true);		
	}
	
	static void reduction(){
		int row,column;
		do{
			System.out.println("Pengurangan");
			System.out.println("***********************************");	
			do{	
				try{
					
					System.out.print("Masukkan jumlah baris matriks : ");
					row=scan.nextInt();
						if (row <1){
							throw new InputMismatchException();
						}
					System.out.print("Masukkan jumlah kolom matriks : ");
					column=scan.nextInt();
						if (column <1){
							throw new InputMismatchException();
						}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan angka lebih besar dari 0");
					scan.nextLine();
					continue;
				}
			}while(true);	
			double[][]matriks1= new double[row][column];
			double[][]matriks2= new double[row][column];
			double[][]hasil=new double[row][column];
			System.out.println("Matriks A");
			do{
				try{
					for(int indeksrow=0; indeksrow<row;indeksrow++){
						for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
							System.out.println("Masukkan nilai baris "+(indeksrow+1)+" dan Kolom "+(indekscolumn+1));
							System.out.print(">> ");
							matriks1[indeksrow][indekscolumn]=scan.nextDouble();
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					scan.nextLine();
					continue;
				}	
			}while(true);			
			System.out.println();
			for(int indeksrow=0; indeksrow<row;indeksrow++){
				for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
					
					System.out.printf("%.2f  ",matriks1[indeksrow][indekscolumn]);
				}System.out.println();
			}	
			System.out.println();		
			System.out.println("Matriks B");
			do{
				try{
					for(int indeksrow=0; indeksrow<row;indeksrow++){
						for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
							System.out.println("Masukkan nilai baris "+(indeksrow+1)+" dan Kolom "+(indekscolumn+1));
							System.out.print(">> ");
							matriks2[indeksrow][indekscolumn]=scan.nextDouble();
							hasil[indeksrow][indekscolumn]= matriks1[indeksrow][indekscolumn] - matriks2[indeksrow][indekscolumn];
						}
					}
					break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan bilangan real");
					scan.nextLine();
					continue;
				}	
			}while(true);		
			System.out.println();
			for(int indeksrow=0; indeksrow<row;indeksrow++){
				for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
					
					System.out.printf("%.2f  ",matriks2[indeksrow][indekscolumn]);
				}System.out.println();
			}		
			System.out.println("\n");
			System.out.println("Hasil pengurangan kedua matriks tersebut adalah : ");
			System.out.println();	
			for(int indeksrow=0; indeksrow<row;indeksrow++){
				for(int indekscolumn=0; indekscolumn<column;indekscolumn++){
					
					System.out.printf("%.2f  ",hasil[indeksrow][indekscolumn]);
				}System.out.println();
			}
			if(row != column){
				System.out.println("Matriks tersebut tidak memiliki determinan");
			}	
			else{
				
				if(row==2 && column==2){
					if(((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0]))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((hasil[0][0]*hasil[1][1])-(hasil[0][1]*hasil[1][0])));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
				else if(row==3 && column==3){
					if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
						System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
								-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
								+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
					}
					else{
						System.out.println("Matriks tersebut merupakan matriks pencerminan");
					}
				}
			}
			System.out.println();
			scan.nextLine();
			do{
				System.out.println("Tekan 1 untuk ulangi dan 0 untuk keluar");
				selec=scan.nextInt();
				if (selec==0){
					 break;
				 }
				 else  if (selec==1){
					 break;
				 }
				 else{
					 System.out.println("Masukkan angka 1 atau 0");
					 continue;
				 }
			}while(true);
			
				 if (selec==0){
					 break;
				 }
				 else  if (selec==1){
					 continue;
				 }		
		}while(true);	
	}
	
	static void multiplication(){	
		while(true){
			 int matriksMultiTotal=0,selec=0,row1,column1,row2,column2;
		 do{
			 do{
				 try{
					 System.out.print("Jumlah baris matriks A: ");
					 row1=scan.nextInt();
					 System.out.print("Jumlah kolom matriks A: ");
					 column1=scan.nextInt();
					 System.out.print("Jumlah baris matriks B: ");
					 row2=scan.nextInt();
					 System.out.print("Jumlah kolom matriks B: ");
					 column2=scan.nextInt();
					 break;
				 }catch(InputMismatchException e){
					 System.out.println("Masukkan bilangan bulat");
					 scan.nextLine();
					 continue;
				 }
			 }while(true); 
					if (column1 != row2) {
						System.out.println("Matriks tidak dapat dikalikan");					
						do{
							try{
								System.out.print("Tekan 1 untuk ulangi dan 0 untuk keluar : ");
								selec=scan.nextInt();
								if (selec==0)break;
								if (selec==1)break;
							}catch(InputMismatchException e){
								System.out.println("Masukkan pilihan yang tersedia");
								scan.nextLine();
								continue;
							}
						}while((selec!=0)||(selec!=1));
					}			
					if (selec==0)break;
					if (selec==1)continue;
		 }while(column1!=row2);
			 if (selec==1)break;
			 System.out.println("\n"); 
		int[][]matriks=new int[row1][column1];
		do{
			System.out.println("Matriks A: ");
			for(int indeksrow=0;indeksrow<row1;indeksrow++){
				for(int indekscolumn=0;indekscolumn<column1;indekscolumn++){
					System.out.print("Baris "+(indeksrow+1)+" column "+(indekscolumn+1)+" : ");
					try{
						matriks[indeksrow][indekscolumn]=scan.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan bulat");
						scan.nextLine();
						indekscolumn--;
						continue;
					}	
				}
			}
			break;
		}while(true);		
		System.out.println();	
		for(int indeksrow=0;indeksrow<row1;indeksrow++){
			for(int indekscolumn=0;indekscolumn<column1;indekscolumn++){
				System.out.printf("%4d ", matriks[indeksrow][indekscolumn]);
			}System.out.println();
		}System.out.println();
		int[][]matriks2=new int[row2][column2];
		do{
			System.out.println("Matriks B: ");
			for(int indeksrow=0;indeksrow<row2;indeksrow++){
				for(int indekscolumn=0;indekscolumn<column2;indekscolumn++){
					System.out.print("row "+(indeksrow+1)+" Kolom "+(indekscolumn+1)+" : ");
					try{
						matriks2[indeksrow][indekscolumn]=scan.nextInt();
					}catch(InputMismatchException e){
						System.out.println("Masukkan bilangan bulat");
						scan.nextLine();
						indekscolumn--;
						continue;
					}
				}
			}
			break;
		}while(true);	
		System.out.println();	
		for(int indeksrow=0;indeksrow<row2;indeksrow++){
			for(int indekscolumn=0;indekscolumn<column2;indekscolumn++){
				System.out.printf("%4d ", matriks2[indeksrow][indekscolumn]);
			}System.out.println();
		}	
		int[][]matriks3=new int[row1][column2];
		for(int indeksrow=0;indeksrow<row1;indeksrow++){
			for(int indekscolumn=0;indekscolumn<column2;indekscolumn++){
				matriksMultiTotal=0;
				for(int multiplicationindex=0;multiplicationindex<column1;multiplicationindex++){
					matriksMultiTotal+=matriks[indeksrow][multiplicationindex]*matriks2[multiplicationindex][indekscolumn];
				}
				matriks3[indeksrow][indekscolumn]=matriksMultiTotal;
					}
				}System.out.println();		
		System.out.println("Perkalian dari matriks "+row1+"x"+column1+" X "+row2+"x"+column2+" Akan membuat matriks "+row1+"x"+column2);
		System.out.println("hasil dari perkalian kedua matriks adalah : ");
		for(int indeksrow=0;indeksrow<row1;indeksrow++){
			for(int indekscolumn=0;indekscolumn<column2;indekscolumn++){
				System.out.printf("%4d ", matriks3[indeksrow][indekscolumn]);
			}
			System.out.println();
		}		
		System.out.println("\n");		
		if(row1 != column2){
			System.out.println("Matriks tersebut tidak memiliki determinan");
		}	
		else{
			if(row1==2 && column2==2){
				if(((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0]))!=0){
					System.out.println("Determinan dari matriks tersebut adalah "+((matriks3[0][0]*matriks3[1][1])-(matriks3[0][1]*matriks3[1][0])));
				}
				else{
					System.out.println("Matriks tersebut merupakan matriks pencerminan");
				}
			}
			else if(row1==3 && column2==3){
				if(((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
						-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
						+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1]))))!=0){
					
					System.out.println("Determinan dari matriks tersebut adalah "+((matriks2[0][0]*((matriks2[1][1]*matriks2[2][2])-(matriks2[2][1]*matriks2[1][2])))
							-(matriks2[0][1]*((matriks2[1][0]*matriks2[2][2])-(matriks2[2][0]*matriks2[1][2])))
							+(matriks2[0][2]*((matriks2[1][0]*matriks2[2][1])-(matriks2[2][0]*matriks2[1][1])))));
				}
				else{
					System.out.println("Matriks tersebut merupakan matriks pencerminan");
				}
			}
		}		
			do{
				try{
					System.out.print("Tekan 1 untuk ulangi dan 0 untuk keluar : ");
					selec=scan.nextInt();
					if (selec==0)break;
					if (selec==1)break;
				}catch(InputMismatchException e){
					System.out.println("Masukkan pilihan yang tersedia");
					scan.nextLine();
					continue;
				}		
			}while((selec<0)||(selec>1));
			if (selec==0)break;
			if(selec==1)continue;
			System.out.println();
		 }
	}
	
	public static void main(String[] args){
			MainMenu();
    }	
}
