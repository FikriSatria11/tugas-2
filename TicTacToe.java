import java.util.*;
public class TicTacToe {

	static Scanner scan = new Scanner(System.in);
	static void play(){
		int number=-1, player=1,choose;
		char[][]tictactoe=new char[3][3];
		for(int row=0;row<3;row++){
			for(int column=0;column<3;column++){
				tictactoe [row][column]=' ';
			}
		}
		do{
			int selec2 = -1;
		do{
			String selec="a";
			for(int row=0;row<3;row++){
				for (int column=0;column<3;column++){
					tictactoe [row][column]=' ';
				}
			}
			System.out.print("Pilih tanda (x/o): ");
			try{
				selec=scan.nextLine();
				if(selec.contentEquals("x")==false && selec.contentEquals("o")==false){
					throw new InputMismatchException();
				}
			}catch(InputMismatchException e){
				System.out.println("~~~~~~~~~~~~~~~~~");
				System.out.println("Masukkan huruf 'x' atau 'o'");
			}
			if (selec.contentEquals("x")){
				number=1;
				break;
			}
			if (selec.contentEquals("o")){
				number=0;
				break;
			}
			
			}while(true);
			
			while(true){
				int isi=0;
				
				System.out.println("------------------------------------");
				System.out.println("\t Giliran pemain ke-"+player);
				System.out.println();
				System.out.println("\t"+tictactoe[0][0]+"|   "+tictactoe[0][1]+"   |"+tictactoe[0][2]);
				System.out.println("\t____|_______|____");
				System.out.println("\t"+tictactoe[1][0]+"|   "+tictactoe[1][1]+"   |"+tictactoe[1][2]);
				System.out.println("\t____|_______|____");
				System.out.println("\t"+tictactoe[2][0]+"|   "+tictactoe[2][1]+"   |"+tictactoe[2][2]);
				System.out.println("\t    |       |    ");
				System.out.println("\n");
				
				if(tictactoe[0][0]!=' '&& tictactoe[0][0]==tictactoe[0][1]&& tictactoe[0][0]==tictactoe[0][2]|| 
				tictactoe[1][0]!=' '&& tictactoe[1][0]==tictactoe[1][1]&&tictactoe[1][0]==tictactoe[1][2]|| 
				tictactoe[2][0]!=' '&& tictactoe[2][0]==tictactoe[2][1]&&tictactoe[2][0]==tictactoe[2][2]||
				tictactoe[0][0]!=' '&& tictactoe[0][0]==tictactoe[1][1]&&tictactoe[0][0]==tictactoe[2][2]||
				tictactoe[0][2]!=' '&& tictactoe[0][2]==tictactoe[1][1]&&tictactoe[0][2]==tictactoe[2][0]||
				tictactoe[0][0]!=' '&& tictactoe[0][0]==tictactoe[1][0]&&tictactoe[0][0]==tictactoe[2][0]||
				tictactoe[0][1]!=' '&& tictactoe[0][1]==tictactoe[1][1]&&tictactoe[0][1]==tictactoe[2][1]||
				tictactoe[0][2]!=' '&& tictactoe[0][2]==tictactoe[1][2]&&tictactoe[0][2]==tictactoe[2][2])
				{
					if (player==1)player++;
					else if(player==2)player--;
					System.out.println("Permainan berakhir, pemain ke-"+player+"Menang");
					do{
						try{
							System.out.print("Tekan 1 untuk bermain lagi dan 0 untuk keluar : ");
							selec2=scan.nextInt();
							
							if((selec2==1)== false && (selec2==0)== false){
								throw new InputMismatchException();
							}
						}
						catch(InputMismatchException e){
							System.out.println("~~~~~~~~~~~~~~~~~~~~~");
							System.out.println("Masukkan Angka 1 atau 0");
							scan.nextLine();
							continue;
						}break;
						}while(true);
						break;
					}
					for(int row=0;row<3;row++){
						for(int column=0;column<3;column++){
						if(tictactoe[row][column]!=' '){
							isi++;
					}
								
				}
					}if(isi==9){
						System.out.println("Permainan berakhir, kedua pemain seri");
						do{
							try{
								System.out.print("Tekan 1 untuk bermain lagi dan 0 untuk keluar");
								selec2=scan.nextInt();
								
								if((selec2==1)==false&&(selec2==0)==false){
									throw new InputMismatchException ();
								}
							}
							catch(InputMismatchException e){
								System.out.println("~~~~~~~~~~~~~~~~~~~~");
								System.out.println("Masukkan angka 1 atau 0");
								scan.nextLine();
								continue;
							}break;
						}while(true);
						break;
						}
					
					System.out.println("Masukkan angka yang tersedia untuk bermain :");
					if(tictactoe[0][0]==' '){
						System.out.println("1. Baris 1 kolom 1");
					}
					if(tictactoe[0][1]==' '){
						System.out.println("2. Baris 1 kolom 2");
					}
					if(tictactoe[0][02]==' '){
						System.out.println("3. Baris 1 kolom 3");
					}
					if(tictactoe[1][0]==' '){
						System.out.println("4. Baris 2 kolom 1");
					}
					if(tictactoe[1][1]==' '){
						System.out.println("5. Baris 2 kolom 2");
					}
					if(tictactoe[1][2]==' '){
						System.out.println("6. Baris 2 kolom 3");
					}
					if(tictactoe[2][0]==' '){
						System.out.println("7. Baris 3 kolom 1");
					}
					if(tictactoe[2][1]==' '){
						System.out.println("8. Baris 3 kolom 2");
					}
					if(tictactoe[2][2]==' '){
						System.out.println("9. Baris 3 kolom 3");
					}
					do{
						
						System.out.print(">>");
						choose=-1;
						try{
							choose=scan.nextInt();
							break;
						}catch(InputMismatchException e){
							System.out.println("Masukkan pilihan yang tersedia");
							scan.nextLine();
							choose=-1;
							continue;
						}
					}while(true);
					
					switch(choose){
					case 1 :
						if(tictactoe[0][0]==' '&&choose==1){
							if(number==1){
								tictactoe[0][0]='x';
								number--;
							}
							else if (number==0){
								tictactoe[0][0]='o';
								number++;
							}
							if(player==1)player++;
							else if(player==2)player--;
						}
						else if(tictactoe[0][0]!=' '&&choose==1){
							System.out.println("Masukkan pilihan diruang yang kosong");
						}break;
					case 2 :
						if(tictactoe[0][1]==' '&&choose==2){
							if(number==1){
								tictactoe[0][1]='x';
								number--;
							}
							else if(number==0){
								tictactoe[0][1]='o';
								number++;
							}
							if(player==1)player++;
							else if(player==2)player--;
						}
						else if(tictactoe[0][1]!=' '&&choose==2){
							System.out.println("Masukkan pilihan diruang yang kosong");	
						}break;
					case 3 :
						if(tictactoe[0][2]!=' '&&choose==3){
							if(number==1){
								tictactoe[0][1]='x';
								number--;
							}
							else if(number==0){
								tictactoe[0][1]='o';
								number++;
							}
							if(player==1)player++;
							else if(player==2)player--;
						}
						else if(tictactoe[0][1]!=' '&&choose==3){
							System.out.println("Masukkan pilihan diruang yang kosong");
						}break;
					case 4 :
						if(tictactoe[1][0]==' '&&choose==4){
							if(number==1){
								tictactoe[1][0]='x';
								number--;	
							}
							else if(number==0){
								tictactoe[1][0]='o';
								number++;
							}
							if(player==1)player++;
							else if(player==2)player--;
						}
						else if (tictactoe[1][0]!=' '&&choose==4){
							System.out.println("Masukkan pilihan diruang yang kosong");
						}break;
					case 5 :
						if(tictactoe[1][1]==' '&&choose==5){
							if(number==1){
								tictactoe[1][1]='x';
								number--;
							}
							else if(number==0){
								tictactoe[1][1]='o';
								number++;
							}
							if(player==1)player++;
							else if(player==2)player--;
						}
						else if (tictactoe[1][0]!=' '&&choose==5){
							System.out.println("Masukkan pilihan diruang yang kosong");
						}break;
					case 6 :
						if(tictactoe[1][2]==' '&&choose==6){
							if(number==1){
								tictactoe[1][2]='x';
								number--;
							}
							else if(number==0){
								tictactoe[1][2]='o';
								number++;
							}
							if(player==1)player++;
							else if(player==2)player--;
						}
						else if (tictactoe[1][2]!=' '&&choose==6){
							System.out.println("Masukkan pilihan diruang yang kosong");
						}break;
					case 7 :
						if(tictactoe[2][0]==' '&&choose==7){
							if(number==1){
								tictactoe[2][0]='x';
								number--;
							}
							else if(number==0){
								tictactoe[2][0]='o';
								number++;
							}
							if(player==1)player++;
							else if(player==2)player--;
						}
						else if (tictactoe[2][0]!=' '&&choose==7){
							System.out.println("Masukkan pilihan diruang yang kosong");
						}break;
					case 8 :
						if(tictactoe[2][1]==' '&&choose==8){
							if(number==1){
								tictactoe[2][1]='x';
								number--;
							}
							else if(number==0){
								tictactoe[2][1]='o';
								number++;
							}
							if(player==1)player++;
							else if(player==2)player--;
						}
						else if (tictactoe[2][1]!=' '&&choose==8){
							System.out.println("Masukkan pilihan diruang yang kosong");
						}break;
					case 9 :
						if(tictactoe[2][2]==' '&&choose==9){
							if(number==1){
								tictactoe[2][2]='x';
								number--;
							}
							else if(number==0){
								tictactoe[2][2]='o';
								number++;
							}
							if(player==1)player++;
							else if(player==2)player--;
						}
						else if (tictactoe[2][2]!=' '&&choose==9){
							System.out.println("Masukkan pilihan diruang yang kosong");
						}break;
						default:
							System.out.println("Masukkan pilihan yang sesuai");
							break;
					}
				}
			if(selec2==1){
				continue;
			}
			else if(selec2==0){
				break;
			}
			
			}while(true);
		}
			public static void main (String[]args){
				do{
					int choose=-1;
					
				System.out.println();
				System.out.println("----------TicTacToe----------");
				System.out.println("1. Main");
				System.out.println("2. Keluar");
				System.out.println("Masukkan pilihan : ");
				try{
					choose=scan.nextInt();
				}catch(InputMismatchException e){
					System.out.println("Masukkan angka 1 atau 0");
				}
				switch(choose){
				case 1 :
					play();
					scan.nextLine();
					break;
				case 0 :
					System.exit(0);
					default:
						System.out.println();
						System.out.println("Masukkan angka 1 atau 0");
						break;
					}
				}while(true);
			}
	}


