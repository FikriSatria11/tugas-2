import java.util.*;
public class dimensi {

	public static void menu(){		
		System.out.println("1. Dua Dimensi");
		System.out.println("2. Tiga Dimensi");
		System.out.println("3. Keluar");
		System.out.println(" Silahkan pilih operasi perhitungan yang diinginkan ");
	}
	public static void Dua_Dimensi(){
		int selec = -1;
		System.out.println("1. Persegi");
		System.out.println("2. Persegi panjang");
		System.out.println("3. Segitiga");
		System.out.println("4. Jajar genjang");
		System.out.println("5. Belah ketupat");
		System.out.println("6. Trapesium");
		System.out.println("7. Lingkaran");
		System.out.println(" Silahkan pilih operasi perhitungan yang diinginkan ");
		Scanner input = new Scanner(System.in);
		
		do{
			try{
				selec=input.nextInt();
				continue;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(selec<1 || selec>7);
		switch (selec){
		case 1 :
			square();
			break;
		case 2 :
			rectangle();
			break;
		case 3 :
			isoscelestriangle();
			break;
		case 4 :
			parallelogram();
			break;
		case 5 :
			rhombus();
			break;
		case 6 :
			trapezoidal();
			break;
		case 7 :
			circle();
			break;
		}
	}
	public static void square(){
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Panjang Sisi :");
		float side;
		do{
			try{
				side=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double area=side*side;
		System.out.println("Luas persegi adalah "+area);
		double circumference = 4*side;
		System.out.println("Keliling persegi adalah "+circumference);
	}
	public static void rectangle(){
		Scanner input = new Scanner(System.in);
		System.out.println("Masukkan Panjang : ");
		float Long;
		System.out.println("Masukkan Lebar : ");
		float wide;
		do{
			try{
				Long=input.nextFloat();
				wide=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double area=Long*wide;
		System.out.println("Luas Persegi Panjang adalah "+area);
		double circumference=(2*Long)+(2*wide);
		System.out.println("Keliling persegi panjang adalah "+circumference);
	}
	public static void isoscelestriangle(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan sisi alas : ");
		float pedestal;
		System.out.println("Masukkan sisi miring : ");
		float hypotenuse;
		System.out.println("Masukkan tinggi : ");
		float high;
		do{
			try{
				pedestal=input.nextFloat();
				hypotenuse=input.nextFloat();
				high=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double area=1/2*(pedestal*high);
		System.out.println("Luas Segitiga adalah "+area);
		double circumference=pedestal+(2*hypotenuse);
		System.out.println("Keliling Segitiga adalah "+circumference);
	}
	public static void parallelogram(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan alas : ");
		float Long;
		System.out.println("Masukkan tinggi : ");
		float high;
		System.out.println("Masukkan sisi miring : ");
		float hypotenuse;
		do{
			try{
				Long=input.nextFloat();
				high=input.nextFloat();
				hypotenuse=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double area=Long*high;
		System.out.println("Luas Jajar Genjang adalah "+area);
		double circumference=(2*Long)+(2*hypotenuse);
		System.out.println("Keliling Jajar Genjang adalah "+circumference);
	}
	public static void rhombus(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan diagonal 1 : ");
		float diagonal1;
		System.out.println("Masukkan diagonal 2 : ");
		float diagonal2;
		System.out.println("Masukkan sisi :");
		float side;
		do{
			try{
				diagonal1=input.nextFloat();
				diagonal2=input.nextFloat();
				side=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double area=1/2*(diagonal1*diagonal2);
		System.out.println("Luas Persegi Panjang adalah "+area);
		double circumference= 4*side;
		System.out.println("Keliling belah Ketupat adalah "+circumference);
	}
	public static void trapezoidal(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan Panjang sisi pertama (sisi sejajar) : alas : ");
		float side1;
		System.out.println("Masukkan panjang sisi kedua (sisi sejajar) : ");
		float side2;
		System.out.println("Masukkan panjang sisi ketiga : ");
		float side3;
		System.out.println("Masukkan panjang sisi keempat : ");
		float side4;
		System.out.println("Masukkan tinggi trapesium : ");
		float high;
		do{
			try{
				side1=input.nextFloat();
				side2=input.nextFloat();
				side3=input.nextFloat();
				side4=input.nextFloat();
				high=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double area = 0.5*(side1+side2)*high;
		System.out.println("Luas Trapesium "+area);
		double circumference = side1+side2+side3+side4;
		System.out.println("Keliling Trapesium adalah "+circumference);
	}
	public static void circle(){
		Scanner input = new Scanner (System.in);
		System.out.println("Masukkan Panjang Jari-jari Lingkaran : ");
		float radius;
		do{
			try{
				radius=input.nextFloat();
				break;
			}
			catch(InputMismatchException e){
				System.out.println("Salah, silahkan input sekali lagi");
				input.nextLine();
				continue;
			}
		}while(true);
		double area = Math.PI*Math.pow(radius, 2);
		System.out.println("Luas Lingkaran "+area);
		double circumference = 2*Math.PI*radius;
		System.out.println("Keliling Lingkaran adalah "+circumference);
			}
			public static void Tiga_Dimensi(){
				int selec = -1;
				System.out.println("1. Tabung");
				System.out.println("2. Balok");
				System.out.println("3. Kubus");
				System.out.println("4. Kerucut");
				System.out.println("5. Bola");
				System.out.println("6. Limas Segiempat");
				System.out.println(" Silahkan pilih operasi perhitungan yang diinginkan ");
				Scanner input = new Scanner(System.in);
				
				do{
					try{
						selec=input.nextInt();
						continue;
					}
					catch(InputMismatchException e){
						System.out.println("Salah, silahkan input sekali lagi");
						input.nextLine();
						continue;
					}
				}while(selec<1 || selec>7);
				switch (selec){
				case 1 :
					tube();
					break;
				case 2 :
					beam();
					break;
				case 3 :
					cube();
					break;
				case 4 :
					cone();
					break;
				case 5 :
					ball();
					break;
				case 6 :
					rectangularpyramid();
					break;
				}
				
			}
			public static void tube(){
				Scanner input = new Scanner (System.in);
				System.out.println("Masukkan Jari-jari : ");
				float radius;
				System.out.println("Masukkan Tinggi Tabung : ");
				float high;
				do{
					try{
						radius=input.nextFloat();
						high=input.nextFloat();
						break;
					}
					catch(InputMismatchException e){
						System.out.println("Salah, silahkan input sekali lagi");
						input.nextLine();
						continue;
					}
				}while(true);
				double area = 2*Math.PI*radius*(radius+high);
				System.out.println("Luas Tabung "+area);
				double Volume = Math.PI*Math.pow(radius,2)*high;
				System.out.println("Volume tabung adalah "+Volume);
			}
				public static void beam(){
					Scanner input = new Scanner (System.in);
					System.out.println("Masukkan Panjang Balok : ");
					float Long;
					System.out.println("Masukkan lebar balok : ");
					float wide;
					System.out.println("Masukkan tinggi balok : ");
					float high;

					do{
						try{
							Long=input.nextFloat();
							wide=input.nextFloat();
							high=input.nextFloat();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Salah, silahkan input sekali lagi");
							input.nextLine();
							continue;
						}
					}while(true);
					double area = 2*((Long*wide)+(Long*high)+(wide+high));
					System.out.println("Luas balok "+area);
					double circumference = 4*(Long+wide+high);
					System.out.println("keliling permukaan balok adalah "+circumference);
					double Volume = Long*wide*high;
					System.out.println("Volume balok adalah "+Volume);
				}
				public static void cube(){
					Scanner input = new Scanner (System.in);
					System.out.println("Masukkan Panjang sisi Kubus : ");
					float side;

					do{
						try{
							side=input.nextFloat();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Salah, silahkan input sekali lagi");
								input.nextLine();
			continue;
						}
					}while(true);
					double area = 6*Math.pow(side,2);
					System.out.println("Luas permukaan kubus"+area);
					double circumference = 12*side;
					System.out.println("keliling permukaan kubus adalah "+circumference);
					double Volume = Math.pow(side,3);
					System.out.println("Volume kubus adalah "+Volume);
				}
				public static void cone(){
					Scanner input = new Scanner (System.in);
					System.out.println("Masukkan jari - jari : ");
					float radius;
					System.out.println("Masukkan tinggi kerucut : ");
					float high;

					do{
						try{
							radius=input.nextFloat();
							high=input.nextFloat();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Salah, silahkan input sekali lagi");
								input.nextLine();
			continue;
						}
					}while(true);
					double area = Math.PI*radius*(radius+Math.sqrt(Math.pow(high,2))+Math.pow(radius, 2));
					System.out.println("Luas kerucut"+area);
					double Volume = 1/3*Math.PI*Math.pow(radius,2)*high;
					System.out.println("Volume kerucut adalah "+Volume);					
				}
				public static void ball(){
					Scanner input = new Scanner (System.in);
					System.out.println("Masukkan jari - jari : ");
					float radius;

					do{
						try{
							radius=input.nextFloat();
							break;
						}
						catch(InputMismatchException e){
							System.out.println("Salah, silahkan input sekali lagi");
								input.nextLine();
			continue;
						}
					}while(true);
					double area = 4*Math.PI*Math.pow(radius,2);
					System.out.println("Luas bola"+area);
					double Volume = 3/4*Math.PI*Math.pow(radius,3);
					System.out.println("Volume bola adalah "+Volume);	
									
					}
					public static void rectangularpyramid(){
						Scanner input = new Scanner (System.in);
						System.out.println("Masukkan Panjang sisi alas : ");
						float side;
						System.out.println("Masukkan Tinggi limas : ");
						float high;
						do{
							try{
								side=input.nextFloat();
								high=input.nextFloat();
								break;
							}
							catch(InputMismatchException e){
								System.out.println("Salah, silahkan input sekali lagi");
									input.nextLine();
				continue;
							}
						}while(true);
						double areabase = Math.pow(side,2);
						double highside = Math.sqrt(Math.pow(0.5*side,2));
						double wideside = 0.5*side*highside;
						double area = areabase+(4*wideside);
						System.out.println("Luas Limas Segiempat"+area);
						double circumference = (4*side)+(4*Math.sqrt(Math.pow(highside,2)+Math.pow(0.5*side,2)));
						System.out.println("Keliling Limas Segiempat"+circumference);
						double Volume = 1/3*areabase*high;
						System.out.println("Volume Limas Segiempat"+Volume);
						
					}
					public static void main (String[]args){
						int selec = -1;
								Scanner input = new Scanner(System.in);
								menu();
								do{
									try{
									selec = input.nextInt();
									break;
									}catch(InputMismatchException e){
										System.out.println("Salah, silahkan input sekali lagi");
										input.nextLine();
										continue;
									}
								}while(true);
								switch(selec){
								case 1:
									Dua_Dimensi();
									break;
								case 2:
									Tiga_Dimensi();
									break;	
								}
										
			}
		}
